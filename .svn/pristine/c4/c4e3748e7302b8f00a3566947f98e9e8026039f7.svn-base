package br.com.indra.earconfig.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public final class ZipUtils {

	//Buffer size default
	private static final int BUFFER_SIZE = 4096;
	public static final String SUFFIX_COPY = "-original";
	
	private ZipUtils(){
	}

	/**
	 * Extrai um arquivo de dentro do Zip.
	 *
	 * @param zipIn
	 *            - Entrada do arquivo no zip
	 * @param filePath
	 *            - Caminho a ser gerado
	 * @param fileName
	 *            - Nome do novo arquivo
	 * 
	 * @throws IOException
	 */
	static public String extractFile(InputStream zipIn, String filePath,
			String fileName) throws IOException {
		return extractFile(zipIn, filePath, fileName, true);
	}
	/**
	 * Extrai um arquivo de dentro do Zip.
	 *
	 * @param zipIn
	 *            - Entrada do arquivo no zip
	 * @param filePath
	 *            - Caminho a ser gerado
	 * @param fileName
	 *            - Nome do novo arquivo
	 * 
	 * @throws IOException
	 */
	static public String extractFile(InputStream zipIn, String filePath,
			String fileName, boolean generateMD5) throws IOException {
		
		String fullFileName = filePath + FileUtils.SEPARATOR + fileName;
		//Se diretorio nao existe, cria
		File directory = new File(new File(fullFileName).getParent());
		if (!directory.exists()){
			directory.mkdirs();
		}
		
		BufferedOutputStream bos = new BufferedOutputStream(
				new FileOutputStream(fullFileName));
		try {
			byte[] bytesIn = new byte[BUFFER_SIZE];
			int read = 0;
			while ((read = zipIn.read(bytesIn)) != -1) {
				bos.write(bytesIn, 0, read);
			}
	    	
		} finally {
			bos.close();
		}
		
		//Gera o MD5
    	if (generateMD5){
    		FileUtils.generateMD5File(fullFileName);
    	}
		//Marca diretorio para remover depois de encerrar o programa
		directory.deleteOnExit();
		
		return fullFileName;
	}
	
	static public void addFileToZip(ZipOutputStream zipOut, String filePath, String fileName) throws IOException {
		String fullFileName = filePath + FileUtils.SEPARATOR + fileName;
		
		zipOut.putNextEntry(new ZipEntry(fileName));
		BufferedInputStream bis = new BufferedInputStream(
				new FileInputStream(fullFileName));
		try {
	        byte[] b = new byte[BUFFER_SIZE];
	        int count;

	        while ((count = bis.read(b)) > 0) {
	            zipOut.write(b, 0, count);
	        }
		} finally {
			bis.close();
		}
	}
	
	static public void addFileToZip(ZipOutputStream zipOut, String filePath, String fileName, String zipEntryName) throws IOException {
		String fullFileName = filePath + FileUtils.SEPARATOR + fileName;
		
		zipOut.putNextEntry(new ZipEntry(zipEntryName));
		BufferedInputStream bis = new BufferedInputStream(
				new FileInputStream(fullFileName));
		try {
	        byte[] b = new byte[BUFFER_SIZE];
	        int count;

	        while ((count = bis.read(b)) > 0) {
	            zipOut.write(b, 0, count);
	        }
		} finally {
			bis.close();
		}
	}
	
	/**
	 * Compacta a pasta informada no arquivo de destino.
	 * 
	 * @param srcFolder - Caminho da pasta a ser compactada
	 * @param destZipFile - Nome completo do arquivo a ser gerado.
	 * 
	 * @throws Exception
	 */
	static public void zipFolder(String srcFolder, String destZipFile)
			throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;

		fileWriter = new FileOutputStream(destZipFile);
		zip = new ZipOutputStream(fileWriter);

		addFolderToZip("", srcFolder, zip, FileUtils.extractLastFolder(srcFolder));
		zip.flush();
		zip.close();
	}

	static private void addFolderToZip(String path, String srcFolder,
			ZipOutputStream zip, String ignorePath) throws Exception {
		File folder = new File(srcFolder+FileUtils.SEPARATOR);

		for (String fileName : folder.list()) {
			if (fileName.isEmpty()){
				return;
			}
			if (path.equals("")) {
				addFileToZip(folder.getName(), srcFolder + FileUtils.SEPARATOR + fileName, zip, ignorePath);
			} else {
				addFileToZip(path + FileUtils.SEPARATOR + folder.getName(), srcFolder + FileUtils.SEPARATOR
						+ fileName, zip, ignorePath);
			}
		}
	}

	static private void addFileToZip(String path, String srcFile,
			ZipOutputStream zip, String ignorePath) throws Exception {
		//Se for um .jar copiado, nao carrega
		if (srcFile.endsWith(SUFFIX_COPY)){
			return;
		}
		if (path.startsWith(ignorePath)){
			path = path.substring(path.indexOf(ignorePath)+ ignorePath.length());
		}
		
		if (path.startsWith(FileUtils.SEPARATOR)) {
			path = path.substring(1);
		}
		
		File folder = new File(srcFile);
		LogUtils.debug("Path - "+path +" | Folder - " + folder.getName());
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip, ignorePath);
		} else {
			if (!path.isEmpty() && !path.endsWith(FileUtils.SEPARATOR)) {
				path = path + FileUtils.SEPARATOR;
			}
			byte[] buf = new byte[1024];
			int len;
			FileInputStream in = new FileInputStream(srcFile);
			zip.putNextEntry(new ZipEntry(path + folder.getName()));
			while ((len = in.read(buf)) > 0) {
				zip.write(buf, 0, len);
			}
			in.close();
		}
	}
	
	 /**
	  * Descompacta um arquivo em uma pasta especifica.
	  * 
	  * @param zipFile - Arquivo Zip a ser descompactado.
	  * @param outputFolder - Pasta de saida
	  */
	static public void unZipIt(String zipFile, String outputFolder) {

		byte[] buffer = new byte[1024];

		try {

			// create output directory is not exists
			File folder = new File(outputFolder);
			if (!folder.exists()) {
				folder.mkdir();
			}

			// get the zip file content
			ZipInputStream zis = new ZipInputStream(
					new FileInputStream(zipFile));
			// get the zipped file list entry
			ZipEntry ze = zis.getNextEntry();

			while (ze != null) {

				String fileName = ze.getName();
				File newFile = new File(outputFolder + File.separator
						+ fileName);

				LogUtils.debug("file unzip : " + newFile.getAbsoluteFile());

				// create all non exists folders
				// else you will hit FileNotFoundException for compressed folder
				new File(newFile.getParent()).mkdirs();

				FileOutputStream fos = new FileOutputStream(newFile);

				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}

				fos.close();
				ze = zis.getNextEntry();
			}

			zis.closeEntry();
			zis.close();

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
