package br.com.indra.earconfig.bean;

import java.util.List;

public class CfgBean {

	private String igualdade; // ":" ou "="
	private String key;
	private List<String> list; // X = a|b|c|d
	private String separacao; // "|" ou "," ou "&" ou ";" ou "null" obs.: "&" e
							  // "," tem chave-valoe

	public String getIgualdade() {
		return igualdade;
	}

	public void setIgualdade(String igualdade) {
		this.igualdade = igualdade;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getSeparacao() {
		return separacao;
	}

	public void setSeparacao(String separacao) {
		this.separacao = separacao;
	}

}
