package br.com.indra.earconfig.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.text.html.HTML.Tag;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.indra.earconfig.bean.CfgBean;
import br.com.indra.earconfig.bean.XmlContent;

public final class FileUtils {

	public static final String SEPARATOR = "/";
	static int status = 0;
	public static ArrayList<XmlContent> xmlContentListOne = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlContentListTwo = new ArrayList<XmlContent>();
	
	public static ArrayList<XmlContent> xmlVerificadorList = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlVerificadorListInversa = new ArrayList<XmlContent>();

	public static ArrayList<CfgBean> cfgContentListOne = new ArrayList<CfgBean>();
	public static ArrayList<CfgBean> cfgContentListTwo = new ArrayList<CfgBean>();
	public static int last_repeat_reference_list;
	public static int last_repeat_target_list;
	public static String tag_xml_value = "";
	public static String xml_referencia;
	public static HashMap<Integer, HashMap<String, String>> FILE_TAG_TO_REJECT = new HashMap<Integer, HashMap<String, String>>();
	private static Document doc;
	
	private FileUtils() {


		xmlContentListOne = null;
		xmlContentListTwo = null;
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		xmlContentListOne.trimToSize();
		xmlContentListTwo.trimToSize();
		status = 0;
		xml_referencia = "";
		FILE_TAG_TO_REJECT.clear();
	}


	static{
	            File fileXml = new File("reject.xml");
	           DocumentBuilderFactory fileFactory = DocumentBuilderFactory.newInstance();
	           try {
	               DocumentBuilder dBuilder = fileFactory.newDocumentBuilder();
	               doc = dBuilder.parse(fileXml);
	               getRejectsMap();
	        } catch (SAXException | IOException | ParserConfigurationException e) {
	            e.printStackTrace();
	        }
	    }

	public static void copy(String src, String dest) throws IOException {
		Files.copy(new File(src).toPath(), new File(dest).toPath(),
				StandardCopyOption.REPLACE_EXISTING);
	}

	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String extractFileNameAndPath(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(0,indexOf);
		  
		} else {
			return fullFileName;
		}
	}
	
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String takeOffFirstDirectoryPath(String fullFileName) {
		int indexOf = fullFileName.indexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(indexOf,fullFileName.length());
		  
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 * getPathName
	 */
	public static String takeOffFirstDirectoryPathPlus(String fullFileName) {
		int indexOf = fullFileName.indexOf(SEPARATOR);
		if (indexOf > 0) {
		  return fullFileName.substring(indexOf+1,fullFileName.length());
		  
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair nome do arquivo informado baseado no ultimo index do
	 * {@link FileUtils.SEPARATOR}.
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo
	 * @return Nome doa arquivo
	 */
	public static String extractFileName(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
			return fullFileName.substring(indexOf + 1);
		} else {
			return fullFileName;
		}
	}
	/**
	 * Extrair pasta do arquivo informado baseado em {@link FileUtils.SEPARATOR}
	 * .
	 * 
	 * @param fullFileName
	 *            - Nome completo do arquivo.
	 * @return Pasta
	 */
	public static String extractFilePath(String fullFileName) {
		int indexOf = fullFileName.lastIndexOf(SEPARATOR);
		if (indexOf > 0) {
			return fullFileName.substring(0, indexOf);
		} else {
			return "";
		}
	}

	/**
	 * Extrair pasta do arquivo informado baseado em {@link FileUtils.SEPARATOR}
	 * .
	 * 
	 * @param fullFileFolder
	 *            - Caminho completo da pasta.
	 * 
	 * @return Nome da ultima Pasta
	 */
	public static String extractLastFolder(String fullFileFolder) {
		return new File(fullFileFolder).getName();
	}

	public static boolean createDirs(String pathToCreate) {
		return new File(pathToCreate).mkdirs();
	}

	public static boolean exists(String pathToCheck) {
		return new File(pathToCheck).exists();
	}

	public static File[] listFilesFromFolder(String folder) {
		File file = new File(folder);
		return file.listFiles();
	}

	/**
	 * M�todo para gera��o de arquivo Properties, respons�vel pelo mapeamento
	 * dos arquivos para os respectivos jars.
	 * 
	 * Al�m disso, possui o par�metro "output" que especifica o nome do arquivo
	 * a ser gerado nos comandos -d e -f.
	 * 
	 * @param configDirectory
	 *            - Diretorio onde o arquivo properties ser� gerado.
	 * @param configFileName
	 *            - Nome de arquivo a ser gerado
	 * @param propertyMap
	 *            - Map de propriedades a serem salvas.
	 */
	public static void savePropertyFile(String configDirectory,
			String configFileName, Map<?, ?> propertyMap) {
		final Properties prop = new Properties() {
			private static final long serialVersionUID = 1L;

			@Override
			public synchronized Enumeration<Object> keys() {
				return Collections.enumeration(new TreeSet<Object>(super
						.keySet()));
			}
		};
		OutputStream output = null;

		try {
			output = new FileOutputStream(configDirectory + FileUtils.SEPARATOR
					+ configFileName);

			prop.putAll(propertyMap);
			// salva properties
			prop.store(output, null);
		} catch (IOException io) {
			io.printStackTrace();
		} finally {
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Carrega arquivo de propriedades da pasta.
	 * 
	 * @param configDirectory
	 *            - Diretorio que arquivo property esta.
	 * @param configFileName
	 *            - Nome do arquivo property.
	 * 
	 * @return Uma entidade de {@link java.util.Properties}.
	 */
	static public Map<String, String> loadPropertyFile(String configDirectory,
			String configFileName) {
		Map<String, String> returnMap = new HashMap<String, String>();
		Properties prop = new Properties();
		InputStream input = null;
		String fullFileName = configDirectory + FileUtils.SEPARATOR
				+ configFileName;
		try {
			input = new FileInputStream(configDirectory + FileUtils.SEPARATOR
					+ configFileName);

			// load a properties file
			prop.load(input);

			for (Entry<Object, Object> entry : prop.entrySet()) {
				returnMap.put((String) entry.getKey(),
						(String) entry.getValue());
			}
		} catch (FileNotFoundException e) {
			LogUtils.fatal("Invalid configuration. Not exists the file - "
					+ fullFileName);
		} catch (IOException ex) {
			LogUtils.fatal("Invalid configuration. It wasn't possible to read the file - "
					+ fullFileName);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return returnMap;
	}

	static public boolean isDirectory(String folderName) {
		return new File(folderName).isDirectory();
	}

	static public void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	static public boolean checkMD5(String configuredFile, String target) {
		try {
			String configuredFileMD5Name = configuredFile + ".MD5";
			LogUtils.debug("MD5 for:" +configuredFileMD5Name);
			if (!new File(configuredFileMD5Name).exists()) {
				LogUtils.debug("No MD5 file for " + configuredFile);
			}
			String md5Target = calculateMD5(target);

			List<String> lines = Files.readAllLines(Paths
					.get(configuredFileMD5Name));

			if (lines.size() != 1) {
				LogUtils.debug("Invalid MD5 for: " + configuredFileMD5Name);
			} else {
				String md5Configurated = lines.get(0);

				if (!md5Configurated.equals(md5Target)) {
					LogUtils.debug("Invalid MD5 check. Inconsistent");

					LogUtils.debug("Configurated: " + md5Configurated);
					LogUtils.debug("Target: " + md5Target);
				} else {
					return true;
				}
			}

			return false;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error checking MD5.");
			return false;
		}
	}

	public static String generateMD5File(String fullFileName) {
		try {
			String md5 = calculateMD5(fullFileName);

			String fullFileNameMD5 = fullFileName + ".MD5";
			FileOutputStream md5OutputStream = new FileOutputStream(
					fullFileNameMD5);
			md5OutputStream.write(md5.getBytes());
			md5OutputStream.close();

			return md5;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error generating MD5 file: " + e.getMessage());
		}
		return null;
	}

	public static String calculateMD5(String fullFileName) {
		try {
			byte[] targetBytes = Files.readAllBytes(Paths.get(fullFileName));
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(targetBytes);

			// convert the byte to hex format method 2
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < thedigest.length; i++) {
				String hex = Integer.toHexString(0xff & thedigest[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			LogUtils.debug(hexString.toString());
//System.out.println("MD5: "+hexString.toString() +"Arquivo: "+fullFileName);
			return hexString.toString();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.debug("Error generating MD5 file: " + e.getMessage());
		}
		return null;
	}
	
	public static String readXmlInputFile(String file, String tag_case){
		
		StringBuilder stringFiles = new StringBuilder();
		
		try {
			File fileXml = new File(file);
			DocumentBuilderFactory fileFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = fileFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fileXml);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName(tag_case);
			System.out.println("\n->Arquivos que servir�o de refer�ncia (xml/properties/cfg): ");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					System.out.println(eElement.getTextContent());
					stringFiles.append(eElement.getTextContent()+",");
				}
			}
			
		} catch (Exception e) {
			System.out.println("Error: "+e.getMessage());
		}
		return stringFiles.toString();
		
	}
	
	
	public static boolean getXmlConfigurationTag(String file, String tag_str){
		boolean override = false;
		try{
			File fxml = new File("configuration.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fxml);
			
			doc.getDocumentElement().normalize();
			
			if(doc.getElementsByTagName(tag_str).item(0).getTextContent().matches("true")){
				override = true;
			}
			
		}catch(Exception ex){
			ex.printStackTrace();
			LogUtils.debug("Error reading XML Configuration file: " + ex.getMessage());			
		}
		
		return override;
	}
	
	
	
	public static String getXmlConfigurationTagValue(String file, String tag_str){
		String textValue = null;
		try{
			File fxml = new File("configuration.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fxml);
			doc.getDocumentElement().normalize();
			textValue = doc.getElementsByTagName(tag_str).item(0).getTextContent();
			
		}catch(Exception ex){
			ex.printStackTrace();
			LogUtils.debug("Error reading XML Configuration file: " + ex.getMessage());			
		}
		
		return textValue;
	}
	
	
	public static String tratarStringPath(String path){
		String new_path = path.replace("_", "/");
		return new_path;
		
	}
	/**
	 * 
	 * @return
	 */
	public static int countMatchesSeparator(String s){
		int counter = 0;
		String[] values = s.split(FileUtils.SEPARATOR);
		counter = values.length - 1;
		for(String value : values){
			if(value.matches("")){
				counter = counter-1;
			}
		}
		///System.out.println("--"+values.length);
		
		return counter;
	}
	// The method to get all children of the dir
    private static File[] listChildrenFiles(File dir)
    {
        List list = new ArrayList();
        listChildrenFiles(dir, list);
        return (File[]) list.toArray(new File[0]);
    }
     
    // The method to recursively get all children of the dir
    private static void listChildrenFiles(File dir, List list)
    {
        File[] files = null;
        if (dir.isDirectory() && ((files = dir.listFiles()) != null))
        {
            for (int i = 0; i < files.length; i++)
                list.add(files[i]);
            for (int i = 0; i < files.length; i++)
                listChildrenFiles(files[i], list);
        }
    }
  
    public static void deleteDirOnExit(File dir)
    {
        File[] files = listChildrenFiles(dir);
         
        // Call deleteOnExit() in reverse order so hope
        // VM will delete the contents first, then delete 
        // the directory
        for (int i=files.length-1; i>=0; i--)
        {
            files[i].deleteOnExit();
        }
  
    }
    
    public static String replaceEmpty(String value){
    	
    	value = value.replace("\r\n", " ").replace("\n", " ").replace("\\s+", " ").trim();
    	value = value.replace("\n", " ");
    	value = value.replace("\\s+", " ");
    	value = value.replaceAll("\\s+", " ");
    	value = value.trim();
    	return value;
    }
	
    public static String replaceRegex(String line, String c){
    	String line_ = line;
    		try {
    			if(line.length()>0 && !c.isEmpty() ){
						Pattern pattern = Pattern.compile(c);
						Matcher matcher = pattern.matcher(line);
						int a = 0;
						int b = 0;
						while (matcher.find()) {
							a = matcher.start();
							b = matcher.end();
						}
						String head = line.substring(0, a);
						String body = line.substring(b, line.length());
						line_ = head+"_";
						if(b < line.length()){
							line_ = line_+body;	
						}
    				}
			} catch (Exception e) {
				System.out.println("Error converting Regex: "+e.getMessage()+" Cause: "+e.getCause());
			}
    	
    		return line_;
    	
    }
    
    public static void getRejectsMap(){
        
    	
    	
    	
        NodeList nListId    = doc.getElementsByTagName("file");
        NodeList nListValue = doc.getElementsByTagName("tag");
       // System.out.println(nListId.getLength());
        for (int temp = 0; temp < nListId.getLength(); temp++) {
              
            Node nNodeId    = nListId.item(temp);
            Node nNodeValue = nListValue.item(temp);
            
            if (nNodeId.getNodeType() == Node.ELEMENT_NODE) {
                  Element eElementId    = (Element) nNodeId;
                  Element eElementValue = (Element) nNodeValue;

                  String file = eElementId.getTextContent().toString();
                  String tag  = eElementValue.getTextContent().toString();
                  HashMap<String, String> rejectMap = new HashMap<String, String>();
                  rejectMap.put(file, tag);
                  
                  FILE_TAG_TO_REJECT.put(temp, rejectMap);
             }
        }
   }
    
    public static boolean containHashMap(String file, String tag){
    	boolean validate = false;
    	try {
			
			for(int i = 0 ; i <= FILE_TAG_TO_REJECT.size(); i ++){
				if(FILE_TAG_TO_REJECT.get(i).containsKey(file) && FILE_TAG_TO_REJECT.get(i).containsValue(tag)){
					validate = true;
				}
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
    	return validate;
    }
    
    
	public static void main(String[] args){
		
		System.out.println("Oi!!!");
		
	}
    
    
}
