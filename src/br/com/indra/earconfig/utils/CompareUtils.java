package br.com.indra.earconfig.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import br.com.indra.earconfig.bean.CfgBean;
import br.com.indra.earconfig.bean.XmlContent;

public class CompareUtils {

	static int status = 0;
	static int k;
	public static ArrayList<XmlContent> xmlContentListOne = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlContentListTwo = new ArrayList<XmlContent>();
	
	public static ArrayList<XmlContent> xmlVerificadorList = new ArrayList<XmlContent>();
	public static ArrayList<XmlContent> xmlVerificadorListInversa = new ArrayList<XmlContent>();

	public static ArrayList<CfgBean> cfgContentListOne = new ArrayList<CfgBean>();
	public static ArrayList<CfgBean> cfgContentListTwo = new ArrayList<CfgBean>();
	public static String xml_referencia;
	public static String xml_reference_name; //only name without path
	
	
	
	public CompareUtils(){
		
		xmlContentListOne = null;
		xmlContentListTwo = null;
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		xmlContentListOne.trimToSize();
		xmlContentListTwo.trimToSize();
		status = 0;
		xml_referencia = "";
		xml_reference_name = "";
	}
	
	
	/**
	 * Verifica se a mudanca foi de um novo elemento (novos elementos-tags XML) ou uma mudanca
	 * apenas de valor de IP ou coisas que nao adicionam
	 * 
	 * @param xml_referencia
	 * @param xml_analisado_novo_ear
	 * @return
	 */
	public static boolean verificarTipoDeDiferencaXML(String xml_referencia, String xml_analisado_novo_ear){
		
		
	//	String xml_analisado_novo_ear_ = xml_analisado_novo_ear.substring(1, xml_analisado_novo_ear.length());
	
		boolean flag_validation = false;
		CompareUtils.xml_referencia = xml_referencia;
		CompareUtils.xml_reference_name = FileUtils.extractFileName(xml_referencia);
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		xmlVerificadorList.clear();
		xmlVerificadorListInversa.clear();
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			docFactory.setValidating(false);
			docFactory.setNamespaceAware(true);
			docFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			docFactory.setFeature("http://xml.org/sax/features/validation", false);
			docFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			docFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.parse(xml_referencia);
			Document doc2 = docBuilder.parse(xml_analisado_novo_ear);

			Element elements = doc.getDocumentElement();
			Element elements2 = doc2.getDocumentElement();
			
			NodeList nodeList = elements.getChildNodes();
			NodeList nodeList2 = elements2.getChildNodes();

			int level = 1;
			
			//System.out.println(":::"+elements.getNodeName() + "[" + level + "]");
			
			XmlContent xmlContentOne = new XmlContent();
			XmlContent xmlContentTwo = new XmlContent();
			
			
			
			xmlContentOne.setLevel(level);
			xmlContentOne.setTag(elements.getNodeName().toString());
			xmlContentOne.setValue(nodeList.item(level).getTextContent());
			xmlContentListOne.add(xmlContentOne);
			
			
			xmlContentTwo.setLevel(level);
			xmlContentTwo.setTag(elements2.getNodeName().toString());
			xmlContentTwo.setValue(nodeList2.item(level).getTextContent());
			xmlContentListTwo.add(xmlContentTwo);
						
			printNodeOne(nodeList, level, xml_reference_name); //= printNode(nodeList, level, xmlContentOne);
			
			if(status == 1){
				printNodeTwo(nodeList2, level, xml_reference_name);
			}
			//System.out.println("tamanhos das listas:"+xmlContentListOne.size()+"--"+xmlContentListTwo.size());
			boolean compareOne = compararListas(xmlContentListOne, xmlContentListTwo);
			boolean compareTwo = compararListasInversamente(xmlContentListTwo, xmlContentListOne);
			//listarTudo(xmlContentListOne, xmlContentListTwo);
			if(compareOne || compareTwo){
				flag_validation = true;
			}
			
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException sae) {
			sae.printStackTrace();
		}
		
		return flag_validation;
	}
	
	
	/**
	 * Metodo respons�vel por alimentar a primeira lista com objetos
	 * @param nodeList
	 * @param level
	 */
	private synchronized static void printNodeOne(NodeList nodeList, int level, String file) {
		level++;
		//String valueNoLineFeed ="";
		String key_ ="";
		String value_ = "";
		if (nodeList != null && nodeList.getLength() > 0) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				
				XmlContent xmlContentOne = new XmlContent();
				Node node = nodeList.item(i);

				value_ = node.getTextContent().toString();
				value_ = FileUtils.replaceEmpty(value_);
				
				key_ = node.getNodeName().toString();
				key_ = FileUtils.replaceEmpty(key_);
				if(value_.length() > 0){
					if(value_.equals("*") || value_.substring(0,1).equals("*")){
						value_ = value_.substring(1,value_.length());
					}
				}
				
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					
					xmlContentOne.setLevel(level);
					xmlContentOne.setTag(key_);
					xmlContentOne.setValue(value_);
					
					//verify if this file-key_ is in reject tag list
					if(!FileUtils.containHashMap(file, key_)){
						xmlContentListOne.add(xmlContentOne);
					}
					
					printNodeOne(node.getChildNodes(), level, xml_reference_name);
				}
			}
		}
		status = 1;
	}
	/**
	 * Metodo respons�vel por alimentar a segunda lista com objetos
	 * @param nodeList
	 * @param level
	 */
	private synchronized static void printNodeTwo(NodeList nodeList, int level, String file) {
		level++;
		//String valueNoLineFeed ="";
		String key_ ="";
		String value_ = "";
		
		if (nodeList != null && nodeList.getLength() > 0) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				XmlContent xmlContentTwo = new XmlContent();
				Node node = nodeList.item(i);
				
				value_ = node.getTextContent().toString();
				value_ = FileUtils.replaceEmpty(value_);
				
				key_ = node.getNodeName().toString();
				key_ = FileUtils.replaceEmpty(key_);
				
				if(value_.length() > 0){
					if(value_.equals("*") || value_.substring(0,1).equals("*")){
						value_ = value_.substring(1,value_.length());
					}
				}
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					xmlContentTwo.setLevel(level);
					xmlContentTwo.setTag(key_);
					xmlContentTwo.setValue(value_);
				
					//verify if this file-key_ is in reject tag list
					if(!FileUtils.containHashMap(file, key_)){
						xmlContentListTwo.add(xmlContentTwo);
					}
					printNodeTwo(node.getChildNodes(), level, xml_reference_name);
				}
			}
		}
		//status = 1;
	}
																//lista3					
	
	
	public static  boolean compararListas(ArrayList<XmlContent> listaA, ArrayList<XmlContent> listaB){

		boolean flag_principal = false;   //recebe o flag do que retornara true se alguma diferenca for encontrada
		boolean flag_first_view = true;   //se for a primeira vez mostra o titulo :::diferencas encontradas:::
		boolean verificador = false;      //verifica se o valor ja existe na lista intermediaria.(lista de repeticoes da propria lista.)
		int level;
		int repeticoes;
		for(int i = 0; i <= listaA.size() - 1; i++){
				String tag = listaA.get(i).getTag();
				String value = listaA.get(i).getValue();
				if(value.length() > 0){
					if(value.equals("*") || value.substring(0,1).equals("*")){
						value = value.substring(0, value.length()); 
					}
				}
				
				level = listaA.get(i).getLevel();
				boolean flag_interno = false;//para colcoar o titulo "mudancas encontradas"
				//int repeticoes;
				//verificador = verificarPreAnalise(tag, value, level);
				verificador = verificarPreAnalise(tag, level);
				if(!verificador){//true - ja existe | false - nao existe na lista
							int repeticoesMesmaLista=0;
							//repeticoes na mesma lista
							for(int h = 0; h <= listaA.size() - 1; h++){
								//if(tag.matches(listaA.get(h).getTag()) && value.matches(listaA.get(h).getValue())){
								if(tag.matches(listaA.get(h).getTag()) && level == listaA.get(h).getLevel()){
									repeticoesMesmaLista++;
								}
							}
							repeticoes=0;
							//repeticoes na lista oposta
							for(int j = 0; j <= listaB.size() - 1; j++){
								//if(tag.matches(listaB.get(j).getTag())  && value.matches(listaB.get(j).getValue())){
								if(tag.matches(listaB.get(j).getTag())  && level == listaB.get(j).getLevel()){
										repeticoes++;
								}
								//finalizou toda a lista
								if(j == listaB.size()-1){
									if((repeticoes != repeticoesMesmaLista)){
										flag_principal = true;
										flag_interno   = true;
										// last_repeat_target_list = repeticoes;
										// last_repeat_reference_list = repeticoesMesmaLista;		
										// tag_xml_value = tag;
									}
		
									
								}
							}
						
							if(flag_interno){
								
								printMsg(flag_first_view, tag, value, repeticoes, repeticoesMesmaLista);
								
								flag_interno = false;
								flag_first_view = false;
							}
				}
				
				insertListValue(tag, value,level,"listaA");
		}

		
		return flag_principal;
	}
	
	
	
	
	public static  boolean compararListasInversamente(ArrayList<XmlContent> listaB, ArrayList<XmlContent> listaA){

		boolean flag_principal = false;
		boolean flag_first_view = true;
		boolean verificador = false;
		int level;
		int repeticoes;
		for(int i = 0; i <= listaB.size() - 1; i++){
				String tag = listaB.get(i).getTag();
				String value = listaB.get(i).getValue();
				if(value.length() > 0){
					if(value.equals("*") || value.substring(0,1).equals("*")){
						value = value.substring(0, value.length()); 
					}
				}
				
				level = listaB.get(i).getLevel();
				boolean flag_interno = false;//para colcoar o titulo "mudancas encontradas"
				//int repeticoes;
				verificador = verificarPreAnaliseInversa(tag, level); 
				if(!verificador){//true - ja existe | false - nao existe na lista
							int repeticoesMesmaLista=0;
							//repeticoes na mesma lista
							for(int h = 0; h <= listaB.size() - 1; h++){
								//if(tag.matches(listaB.get(h).getTag()) && value.matches(listaB.get(h).getValue())){
								if(tag.matches(listaB.get(h).getTag()) && level == listaB.get(h).getLevel()){
									repeticoesMesmaLista++;
								}
							}
							repeticoes=0;
							//repeticoes na lista oposta
							for(int j = 0; j <= listaA.size() - 1; j++){
								//if(tag.matches(listaA.get(j).getTag())  && value.matches(listaA.get(j).getValue())){
								if(tag.matches(listaA.get(j).getTag())  && level == listaA.get(j).getLevel()){
										repeticoes++;
								}
								//finalizou toda a lista
								if(j == listaA.size()-1){
									if((repeticoes != repeticoesMesmaLista)){
										
										flag_principal = true;
										flag_interno   = true;
										// last_repeat_target_list = repeticoes;
										// last_repeat_reference_list = repeticoesMesmaLista;		
										// tag_xml_value = tag;
									}
		
									
								}
							}
						
							if(flag_interno){
								
								printMsg(flag_first_view, tag, value, repeticoes, repeticoesMesmaLista);
								
								flag_interno = false;
								flag_first_view = false;
							}
				}
				
				insertListValue(tag, value,level,"listaB");
		}

		
		return flag_principal;
	}
	
	
	
	public static void printMsg(boolean flag_first_view, String tag, String value, int repeticoes, int repeticoesMesmaLista){
		
		try {
				if(!value.matches("")){
						if(flag_first_view){
								System.out.println("\n\n:::::::::::Mudanca encontrada:::::::::::\nArquivo:"+xml_referencia+
										"\n No arquivo de referencia o elemento <"+tag+"> com valor igual \""+value+"\" aparece "+repeticoesMesmaLista+" vez(es)."+
										"\n Mas no arquivo target elemento <"+tag+"> com valor igual \""+value+"\" aparece "+repeticoes+" vez(es).");
						}else{
							System.out.println("No arquivo de referencia o elemento <"+tag+"> com valor igual \""+value+"\" aparece "+repeticoesMesmaLista+" vez(es)."+
									"\n Mas no arquivo target elemento <"+tag+"> com valor igual \""+value+"\" aparece "+repeticoes+" vez(es).");
						}
				}else{
						    if(flag_first_view){
							System.out.println("\n\n:::::::::::Mudanca encontrada:::::::::::\nArquivo:"+xml_referencia+
									"\n No arquivo de referencia o elemento <"+tag+"> aparece "+repeticoesMesmaLista+" vez(es)."+
									"\n Mas no arquivo target elemento <"+tag+"> aparece "+repeticoes+" vez(es).");
							}else{
							System.out.println("No arquivo de referencia o elemento <"+tag+"> aparece "+repeticoesMesmaLista+" vez(es)."+
									"\n Mas no arquivo target elemento <"+tag+"> aparece "+repeticoes+" vez(es).");
							}
				}
				
		} catch (Exception e) {
			e.getStackTrace();
		}
		
	}
	
	
	
	
	public static void insertListValue(String tag,String  value, int level, String lista){
		
		XmlContent xmlc = new XmlContent();
		xmlc.setLevel(level);
		xmlc.setTag(tag);
		xmlc.setValue(value);
		
		if(lista.matches("listaA")){
			
			xmlVerificadorList.add(xmlc);
		}else{
			xmlVerificadorListInversa.add(xmlc);
		}
		
	}
	
	
	
	/**
	 * metodo que verifica se elemento ja sofreu analise de repeticao
	 * caso ja tenha sido analisado retorna true;
	 * @return
	 */
	public static boolean verificarPreAnalise(String tag,/*String  value,*/ int level){
		boolean verificador = false;
		try {
			for(XmlContent xml : xmlVerificadorList){
				//System.out.println("Valor existente: "+xml.getValue()+" e Tamanho da lista: "+xmlVerificadorList.size());
				if(xml.getTag().matches(tag)  && xml.getTag().matches(tag)){
					verificador = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error:::"+e.getMessage()+"--"+e.getCause());
		}
		return verificador;
	}
	/**
	 * metodo que verifica se elemento ja sofreu analise de repeticao
	 * caso ja tenha sido analisado retorna true;
	 * @return
	 */
	public static boolean verificarPreAnaliseInversa(String tag,/*String  value,*/ int level){
		boolean verificador = false;
		try {
			for(XmlContent xml : xmlVerificadorListInversa){
				//System.out.println("Valor existente: "+xml.getValue()+" e Tamanho da lista: "+xmlVerificadorListInversa.size());
				//if(xml.getValue().matches(value) && xml.getLevel() == level && xml.getTag().matches(tag)){
				if(xml.getLevel() == level && xml.getTag().matches(tag)){
					verificador = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error:::"+e.getMessage()+"--"+e.getCause());
		}
		return verificador;
	}	
	
	
	public static void listarTudo(ArrayList<XmlContent> listaA, ArrayList<XmlContent> listaB){
		for(int i = 0; i <= listaA.size() - 1; i++){
			System.out.println("Valores da lista A-->" +listaA.get(i).getTag());
		}
		System.out.println("\n\n");
		for(int j = 0; j <= listaB.size() - 1; j++){
			System.out.println("Valores da lista B-->" +listaB.get(j).getTag());
		}
	}
	
	public static boolean verificarTipoDeDiferencaPROPERTIES(String properties_referencia, String properties_analisado_novo_ear){
		boolean validator = false;
		CompareUtils.xml_referencia = properties_referencia;
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		try{
			if(properties_referencia.endsWith(".properties")){
				readAndSaveList1(properties_referencia);
				if(status == 1){
					readAndSaveList2(properties_analisado_novo_ear);
				}
				//listarTudo(xmlContentListOne, xmlContentListTwo);
				boolean compareOne = compararDuasListasProperties(xmlContentListOne, xmlContentListTwo);
				boolean compareTwo = compararDuasListasInversamenteProperties(xmlContentListOne, xmlContentListTwo);
				if(compareOne | compareTwo){
					validator = true;
				}
			}
		}catch(Exception ex){
			System.out.println("\nFalha no arquivo properties. ");
		}
		
		return validator;
	}
	
	
	public static boolean verificarTipoDeDiferencaCFG(String cfg_referencia, String cfg_analisado_novo_ear){
		boolean validator = false;
		CompareUtils.xml_referencia = cfg_referencia;
		xmlContentListOne.clear();
		xmlContentListTwo.clear();
		try{
			if(cfg_referencia.endsWith("cfg")){
				readAndSaveList3(cfg_referencia, 1);//numero 1 para diferenciar listas e evitar problemas de monothread
				if(status == 1){
				try {
					readAndSaveList3(cfg_analisado_novo_ear, 2);
				} catch (Exception e) {
					System.out.println("\nFalha no arquivo CFG.. "+e.getMessage()+"-"+e.getCause());
				}
					
				}
				boolean compareOne = compararDuasListasCFG(cfgContentListOne, cfgContentListTwo);//true contem diferencas
				boolean compareTwo = compararDuasListasCFG(cfgContentListTwo, cfgContentListOne);//true contem diferencas
				
				if(compareOne | compareTwo){
					validator = true;
				}
			}
		}catch(Exception ex){
			System.out.println("\nFalha no arquivo CFG. "+ex.getMessage()+"-"+ex.getCause());
		}
		
		return validator;
	}
	
	public static void readAndSaveList1(String properties_file){
		//System.out.println("valor do properties: "+properties_file);
		try {
			File file = new File(properties_file);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			StringBuffer stringBuffer = new StringBuffer();
			String line;
			String key = null;
			String value = null;
			while ((line = bufferedReader.readLine()) != null) {
				int posicao_do_igual = line.indexOf("=");
				int count = StringUtils.countMatches(line,"=");
				if(posicao_do_igual != -1 && count == 1){
					//System.out.println("numero de h's: "+h+"---"+count);
					key = line.substring(0,line.lastIndexOf("="));
					value = line.substring(posicao_do_igual + 1,line.length());
					XmlContent xmlContent = new XmlContent();
					xmlContent.setTag(key);
					xmlContent.setValue(value);
					xmlContentListOne.add(xmlContent);
					
				}else{
					//System.out.println("Elementos invalido dentro do arquivo properties!");
				}

			}
			status = 1;
			fileReader.close();
			//System.out.println("Contents of file:");
			//System.out.println(stringBuffer.toString());
		} catch (IOException e) {
			System.out.println("Falha: "+e.getMessage()+"-"+e.getCause());
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void readAndSaveList3(String cfg_file, int flag){
		try {
			File file = new File(cfg_file);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			CfgAuditor cfgBean = new CfgAuditor();
			while ((line = bufferedReader.readLine()) != null) {
			if(line.trim().startsWith("#")){	

			    }else if(line.trim().length() != 0){
				   if(line.indexOf("{") > 0){
						line = FileUtils.replaceRegex(line,"\\{");
					}
					if(line.indexOf("}") > 0){
						line = FileUtils.replaceRegex(line,"\\}");
					}
					cfgBean.verificarCaso(line.trim().replaceAll("\\s", ""));
				}
			}
				if(flag == 1){
					cfgContentListOne = cfgBean.retornarListaObjetos();
					status = 1;
				}else{
					cfgContentListTwo = cfgBean.retornarListaObjetos();
				}
			
			fileReader.close();
		} catch (IOException e) {
			System.out.println("Error>>>>"+e.getMessage()+"-"+e.getCause());
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public static void readAndSaveList2(String properties_file){
		
		try {
			File file = new File(properties_file);
			FileReader fileReader = null;
			if(file.exists()){
				 fileReader = new FileReader(file);
			}
			
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			//StringBuffer stringBuffer = new StringBuffer();
			String line;
			String key = null;
			String value = null;
			while ((line = bufferedReader.readLine()) != null) {
				int posicao_do_igual = line.indexOf("=");
				int count = StringUtils.countMatches(line,"=");
				if(posicao_do_igual != -1 && count == 1){
					//System.out.println("numero de h's: "+h+"---"+count);
					key = line.substring(0,line.lastIndexOf("="));
					value = line.substring(posicao_do_igual + 1,line.length());
					XmlContent xmlContent = new XmlContent();
					xmlContent.setTag(key);
					xmlContent.setValue(value);
					xmlContentListTwo.add(xmlContent);
				}else{
					//System.out.println("Linha nao considerada!!");
				}
			
			}
			fileReader.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public static  boolean compararDuasListasProperties(ArrayList<XmlContent> listaA, ArrayList<XmlContent> listaB){

		boolean flag_lista_One = false; //true se notar diferencas...
		boolean flag_first_view = true;
		int level;
		int repeticoes;
		try {
			
		
		for(int i = 0; i <= listaA.size() - 1; i++){
				String tag = listaA.get(i).getTag();
				level = listaA.get(i).getLevel();
				boolean flag_interno = false;
				//int repeticoes;
				int repeticoesMesmaLista=0;
				for(int h = 0; h <= listaA.size() - 1; h++){
					if(tag.matches(listaA.get(h).getTag())){
						repeticoesMesmaLista++;
					}
				}
				repeticoes=0;
				for(int j = 0; j <= listaB.size() - 1; j++){
					if(tag.matches(listaB.get(j).getTag())){
						repeticoes++;
					}
					//verifica que quanto existe a mais de um em relacao ao outro
					//tendo ambos os mesmos elementos, mas em qauntidades diferentes.
					if(j == listaB.size()-1){
						if(repeticoes != repeticoesMesmaLista){
							//System.out.println("PROPERTIES: Tem em ambos, mas um tem mais que outro!!"+tag+"-->"+repeticoes+" Versus "+repeticoesMesmaLista);
							 flag_lista_One = true;
							 flag_interno = true;
						}
						
					}
				}
			
				if(flag_interno){
					
					printMsg(flag_first_view, tag, "", repeticoes, repeticoesMesmaLista);
					flag_interno = false;
					flag_first_view = false;
				}
		}
		} catch (Exception e) {
			System.out.println("Error...>"+e.getMessage()+"-"+e.getCause());
		}
		return flag_lista_One;
	}

	
	public static  boolean compararDuasListasInversamenteProperties(ArrayList<XmlContent> listaA, ArrayList<XmlContent> listaB){

		boolean flag_lista_two = false; //flag principal. Uma alteracao de funcionalidade ja indica true.
		boolean flag_first_view = true; //indica se � a 1a vez que encontrou diferenca e coloca o header do display uma unica vez. (:::mudanca encotnradas:::) 
		//String tag = "";
		int level;
		//ArrayList<String> cc = new ArrayList<String>();
		
		for(int i = 0; i <= listaB.size() - 1; i++){
				String tag = listaB.get(i).getTag();
				level = listaB.get(i).getLevel();
				int repeticoes;
				int repeticoesMesmaLista=0;
				boolean flag_interno = false;
				for(int h = 0; h <= listaB.size() - 1; h++){
					if(tag.matches(listaB.get(h).getTag())){
					//	System.out.println("Na mesma lista da referencia "+tag+" -- "+listaA.get(h).getTag());
						repeticoesMesmaLista++;
					}
				}
				//System.out.println("valor de 'repeticoesMesmaLista' "+repeticoesMesmaLista+".."+tag);
				repeticoes=0;
				for(int j = 0; j <= listaA.size() - 1; j++){
					if(tag.matches(listaA.get(j).getTag())){
						repeticoes++;
					}
					//verifica que quanto existe a mais de um em relacao ao outro
					//tendo ambos os mesmos elementos, mas em qauntidades diferentes.
					if((j == listaA.size()-1) && (repeticoes != repeticoesMesmaLista)){
							flag_lista_two = true;
							flag_interno = true;

					}
					
				}
				if(flag_interno){
					
					printMsg(flag_first_view, tag, "", repeticoes, repeticoesMesmaLista);
					flag_interno = false;
					flag_first_view = false;
				}
			
		}
		return flag_lista_two;
	}
	
	
	public static  boolean compararDuasListasCFG(ArrayList<CfgBean> listaA, ArrayList<CfgBean> listaB){

		boolean flag_lista_two = false;
		boolean flag_interno = false;
		boolean flag_first_view = true; //indica se � a 1a vez que encontrou diferenca e coloca o header do display uma unica vez. (:::mudanca encotnradas:::)
		int repeticoes;
	
		for(int i = 0; i <= listaB.size() - 1; i++){//verificacao na mesma lista
				//key ->cabenca da lista(caso exista)
				//lista -> numero de elementos  na lista
				//verificar igualdade
				String key = listaB.get(i).getKey();
				String igualdade = listaB.get(i).getIgualdade();
				List<String> listValue = listaB.get(i).getList();
				int repeticoesMesmaLista = 0;
				repeticoes = 0;
				flag_interno = false;
				for(int h = 0; h <= listaB.size() - 1; h++){
	
				//	System.out.println("key:"+key+"--"+listaB.get(h).getKey());
					if(key.matches(listaB.get(h).getKey()) &&  igualdade.matches(listaB.get(h).getIgualdade())){
						repeticoesMesmaLista++;
					}
				}
				for(int j = 0; j <= listaA.size() - 1; j++){

							if(key.matches(listaA.get(j).getKey()) && igualdade.matches(listaA.get(j).getIgualdade())){
										repeticoes++;
							}
							if((j == listaA.size()-1) && (repeticoes != repeticoesMesmaLista)){
									flag_lista_two = true;
									flag_interno = true;

							
							}


				}
				if(flag_interno){
					
					printMsg(flag_first_view, key, "", repeticoes, repeticoesMesmaLista);
					
					flag_interno = false;
					flag_first_view = false;
					flag_lista_two = true;
				}
				

		
	}
		return flag_lista_two;
}
	
	public static  boolean compararDuasListasInversamenteCFG(ArrayList<CfgBean> listaA, ArrayList<CfgBean> listaB){

		boolean flag_lista_two = false;
		boolean verifica = false;
	
		for(int i = 0; i <= listaA.size() - 1; i++){//verificacao na mesma lista
				String key = listaA.get(i).getKey();
				String igualdade = listaA.get(i).getIgualdade();
				verifica = false;
				for(int j = 0; j <= listaB.size() - 1; j++){
					
							if(key.matches(listaB.get(j).getKey()) && igualdade.matches(listaA.get(j).getIgualdade())){
										verifica = true;
							}
							if(j == listaB.size() - 1 && !verifica){
									System.out.println("\n\n:::::::::::Mudanca encontrada:::::::::::\nArquivo:"+xml_referencia+
										"\n No arquivo de referencia o elemento "+key+" nao existe."+
										"\n Mas no arquivo target o elemento "+key+" existe.\n\n");
							}

				}
		
	}
		return flag_lista_two;
}

}
