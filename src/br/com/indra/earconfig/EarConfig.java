package br.com.indra.earconfig;

import java.util.Map;

import org.docopt.Docopt;

import br.com.indra.earconfig.process.ConfigEAR;

/**
 * @TODO Criar processo para "test" e "update".
 * 
 */
public class EarConfig {
	private static final String doc = "earconfig.\n"
			+ "\n"
			+ "Usage:\n"
			+ "  earconfig create (--dir=<name> --client=<name> | --dir=<name>) <earfile>\n"
			//+ "  earconfig test (--file=<name> | --dir=<name>) <earfile>\n"
			//+ "  earconfig update (--file=<name> | --dir=<name>) <earfile>\n"
			+ "  earconfig apply (--file=<name> --output=<name> | --dir=<name>  --output=<name>) <earfile>\n"
			+ "  earconfig (-h | --help)\n"
			+ "  earconfig --version\n"
			+ "\n"
			+ "Options:\n"
			+ "  create          Cria pasta (--dir) ou arquivo (--file) com a configuração de referência utilizando os arquivos originais do EAR.\n"
			//+ "  test          Testa a configuração de referência e compara o MD5 dos arquivos de configuração com o MD5 dos arquivos do EAR.\n"
			//+ "  update        Atualiza a configuração de referência com o novo MD5 dos arquivos do EAR.\n"
			+ "  apply            Cria um novo arquivo EAR aplicando os arquivos de configuração da configuração de referência.\n"
			+ "  -h --help        Mostra esta mensagem.\n"
			+ "  --version        Mostra a versão.\n"
			+ "  --file=<name>    Nome do arquivo zip ou jar que contém as configurações.\n"
			+ "  --dir=<name>     Nome da pasta que contém as configurações.\n"
			+ "  --output=<name>  Nome da pasta onde o novo arquivo será gerado.\n"
			//+ "  --client=<name>  Parâmetro (true ou false) para ativar modo client de criação de arquivos de referância.\n"
			+ "  <earfile>        Caminho completo do arquivo EAR (ou JAR) a ser utilizado";

	public static void main(String[] args) {
		Map<String, Object> opts = new Docopt(doc).withVersion("EarConfig 1.5")
				.parse(args);
		System.out.println(opts);
		
		ConfigEAR configEar = new ConfigEAR();
		configEar.process(opts);
		//System.out.println("Processo finalizado!!!");
		//Success
		System.exit(0);
	}
}
