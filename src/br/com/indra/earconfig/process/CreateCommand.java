package br.com.indra.earconfig.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FilenameUtils;

import br.com.indra.earconfig.utils.FileUtils;
import br.com.indra.earconfig.utils.LogUtils;
import br.com.indra.earconfig.utils.ZipUtils;

public final class CreateCommand {
	
	public static final String Properties_file = "input.xml";
	public String FILES_TO_READ = "";                                
	public String CLIENT_JARS_TO_READ = ""; 
	public boolean first_time = true; //este flag eh necessario para que arquivos JAR consigam ser esxtraidos assim como EAR e extraia os sub projetos JARs, caso existam.
	public static final String Configuration_file = "configuration.xml";
	public boolean SHOW_FILES = false;
	public boolean CLIENT_EVENT = false;
	private final Map<String, String> propertyMap = new HashMap<String, String>();
	private final List<String> acceptFilesList = new ArrayList<String>();
	private final List<String> acceptJARFilesList = new ArrayList<String>();
	private Charset ENCODE = null;
	//private static Charset windows_1252 = Charset.forName("windows-1252");//defaultCharset();
	//private static Charset UTF8         = Charset.forName("UTF-8");//defaultCharset();
	//private static Charset CP866        = Charset.forName("CP866");  //("CP866");
																	 //CP437 
	public CreateCommand() {
		if(FileUtils.exists(Properties_file)){
			//aqui a aplicacao obtem os arquivos xml que serao scaneados.
			this.FILES_TO_READ = FileUtils.readXmlInputFile(Properties_file, "file");
			//this.CLIENT_JARS_TO_READ = FileUtils.readXmlInputFile(Properties_file, "jar");
			this.ENCODE = Charset.forName(FileUtils.getXmlConfigurationTagValue(Configuration_file, "encode"));
		}else{
			System.out.println("\n\narquivo input.xml nao existe ou inacessivel!!!\n\n");
		}
		
		if(FileUtils.exists(Configuration_file)){
			if(FileUtils.getXmlConfigurationTag(Configuration_file, "showUnzipedFiles")){
				this.SHOW_FILES = true;
			}
		}
		acceptFilesList.clear();
		acceptFilesList.addAll(Arrays.asList(FILES_TO_READ.split(",")));
		acceptJARFilesList.clear();
		acceptJARFilesList.addAll(Arrays.asList(CLIENT_JARS_TO_READ.split(",")));
		
		System.out.println("\n\nLista adicionada eh memoria.\nAguarde ...");
	}
	/**
	 *
	 * @param directory
	 * @param earFile
	 * @param create_type - pode ser cliente (true) ou server (false)
	 */
	public void process(String directory, String earFile, boolean create_type){
				if(create_type){
					this.CLIENT_EVENT = true;
				}
		
				String extension = FilenameUtils.getExtension(earFile);
				String extension_configuration = "";
				if(extension.matches("ear")){
					extension_configuration = ".ear";
				}
				if(extension.matches("jar")){
					extension_configuration = ".jar";
				}
				if(extension.matches("zip")){
					extension_configuration = ".zip";
				}
				if(extension.matches("war")){
					extension_configuration = ".war";
				}
				if(extension.matches("rar")){//nao eh o "rar" famoso! Eh o "rar" da Oracle.
					extension_configuration = ".rar";
				}
				if (!FileUtils.exists(directory)){
					FileUtils.createDirs(directory);
				}
				
				try {
					FileUtils.delete(new File(directory));
				} catch (IOException e) {
					e.printStackTrace();
					LogUtils.fatal("Error cleaning config folder. Check if has no opened files from: "+directory);
				}
				propertyMap.clear();
				propertyMap.put("output", FileUtils.extractFileName(earFile).replace(extension_configuration, "_")+ FileUtils.extractFileName(directory)+ extension_configuration);
				System.out.println("Extraindo arquivos de referencia.\nAguarde ...");
				
				if(CLIENT_EVENT){
					unzip(earFile, earFile, directory);
				}else{
					unzip(earFile, earFile, directory);
				}
				
				FileUtils.savePropertyFile(directory,ConfigEAR.EAR_CONFIG_FILENAME, propertyMap);
				System.out.println("Arquivos de referencia criados com sucesso!");
				System.out.println("\nAplicacao finalizada com sucesso!");
	}
	
	/**
	 * Extrai um aruivo ZIP especificado em zipFilePath para o diretorio
	 * especificado em destDirectory (cria o diretorio caso nao exista)
	 *
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	private void unzip(String zipFileReference, String zipFilePath, String destDirectory) {
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		String projectName = "";
		String jarFileName = "";
		String ext = FilenameUtils.getExtension(zipFilePath);//jar ou rar ou ear ou zip ou jar etc
		try {
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath),this.ENCODE);
			if (zipFilePath.endsWith(ext)) {
				jarFileName = zipFilePath.replace(destDirectory	+ FileUtils.SEPARATOR, "");
				projectName = FileUtils.extractFileName(zipFilePath).replace("."+ext, "_");
				this.first_time = false;
			}
			
			ZipEntry entry = zipIn.getNextEntry();
			while (entry != null) {
				String entryFileName = FileUtils.extractFileName(entry.getName());
				String extension = FilenameUtils.getExtension(entryFileName);
				String finalName = "";
				if(SHOW_FILES && !extension.matches("class")){
					System.out.println("Arquivos encontrados no arquivo comprimido: "+entry.getName());
				}
//				if((FileUtils.extractFileName(entry.getName())).matches("application.xml")){
//					System.out.println(entry.getName());
//				}
				//se existe na lista de arquivos aceitos
				if (acceptFilesList.indexOf(entryFileName) >= 0) {
					String extractFileName = null;
					//if(FileUtils.countMatchesSeparator(zipFilePath) > 1){
						
						if (FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) 
								&& acceptFilesList.indexOf(entryFileName) >= 0) {
					
							String j = FileUtils.extractFileNameAndPath(jarFileName);
							j = j.replace(FileUtils.SEPARATOR, "_");
							
							//extractFileName = j + "_" + projectName + entry.getName().replace(FileUtils.SEPARATOR, "_");
							extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");
							finalName = entry.getName();
							//System.out.println("Arquivo solto!"+entry.getName());
							
						}else{
							extractFileName = projectName + entry.getName().replace(FileUtils.SEPARATOR, "_");
							//if((jarFileName.substring(0,1)).matches(FileUtils.SEPARATOR)){
							//	finalName = jarFileName.substring(1) + FileUtils.SEPARATOR + entry.getName();
							//}else{
								finalName = jarFileName + FileUtils.SEPARATOR + entry.getName();
							//}
							
							//System.out.println("Arquivo dentro de outro jar!"+entry.getName());
						}
						
					
						
					//}else{
						
					//}
					
					propertyMap.put(extractFileName, finalName);
					//System.out.println(entry.getName());
					
					ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
				}
				else if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar") ||
						 entry.getName().endsWith("war") || entry.getName().endsWith("ear") ||
						 entry.getName().endsWith("zip")) {

							String entryFilePath = FileUtils.extractFilePath(entry.getName());
							// Armazena nome completo do arquivo extraido
							String fullFileName = ZipUtils.extractFile(zipIn, destDirectory
									+ FileUtils.SEPARATOR + entryFilePath,
									entryFileName, true);
							
							//Descompacta o arquivo zip extraido.
							unzip(zipFileReference,fullFileName, destDirectory);
							new File(fullFileName).deleteOnExit();
							
							File md5File = new File(fullFileName+".MD5");
							if (md5File.exists()){
								md5File.deleteOnExit();
							}
			   
				}
				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
				this.first_time = false;
			}
			zipIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzip the EAR file -> "
					+ e.getMessage());
		}
	}
	
	/**
	 * Extrai um arquivo ZIP especificado em zipFilePath para o diretorio
	 * especificado em destDirectory (cria o diretorio caso nao exista)
	 *
	 * @param zipFilePath
	 * @param destDirectory
	 * @throws IOException
	 */
	private void unzipClient(String zipFileReference, String zipFilePath, String destDirectory) {
		//System.out.println("entrei no metodo unzipClient!");
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		String projectName = "";
		String jarFileName = "";

		try {
			//Charset CP866 = Charset.forName("CP866");
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath),this.ENCODE);
			String extensionFile = FilenameUtils.getExtension(zipFilePath);
			
			if (zipFilePath.endsWith("jar") || zipFilePath.endsWith("rar")|| zipFilePath.endsWith("war") 
					|| zipFilePath.endsWith("zip")	|| zipFilePath.endsWith("ear")) {
				
				jarFileName = zipFilePath.replace(destDirectory	+ FileUtils.SEPARATOR, "");
				projectName = FileUtils.extractFileName(zipFilePath).replace("."+extensionFile, "_");
			
			}
			ZipEntry entry = zipIn.getNextEntry();
			while (entry != null) {
				
				String extractFileName = null;
				String nameFile = null;
				
				if(FileUtils.extractFileName(entry.getName()).matches("zeus.ini")){
					System.out.println("ok");
				}
				if(FileUtils.extractFileName(entry.getName()).matches("w10.properties")){
					System.out.println("ok");
				}
				if(FileUtils.extractFileName(entry.getName()).matches("notifiers.xml")){
					System.out.println("ok2");
				}
				if(FileUtils.extractFileName(entry.getName()).matches("zeus.cfg")){
					System.out.println("ok3");
				}
				String entryFileName = FileUtils.extractFileName(entry.getName());
				String extension = FilenameUtils.getExtension(entryFileName);
				if(SHOW_FILES && !extension.matches("class")){
					System.out.println("Arquivos encontrados no EAR: "+entry.getName());
				}
				//verifica se é arquivo solto do diretorio principal
				if (FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) 
						&& acceptFilesList.indexOf(entryFileName) >= 0) {
						//this.first_time = false;
						nameFile = FileUtils.takeOffFirstDirectoryPath(entry.getName());
//						if(FileUtils.extractFileName(entry.getName()).matches("application.xml")){
//							System.out.println("Arquivo analisado: "+entry.getName());
//						}
						
						String j = FileUtils.extractFileNameAndPath(jarFileName);
						String w = FileUtils.extractFileName(jarFileName);
						j = j.replace(FileUtils.SEPARATOR, "_");
						//extractFileName = FileUtils.takeOffFirstDirectoryPathPlus(entry.getName()).replace(FileUtils.SEPARATOR, "_");//versao cliente antiga
						extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");//versao cliente antiga
						//nameFile = w+FileUtils.SEPARATOR+nameFile;
						//extractFileName = entry.getName().replace(FileUtils.SEPARATOR, "_");//versao nova tentativa de funcionamento
						//	System.out.println("Arquivo geradoA: "+extractFileName);
						propertyMap.put(extractFileName, entry.getName());//versao nova tentaiva de funcioanmento
						ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
						System.out.println(nameFile);
				//entra aqui caso não seja arquivo na pasta principal		
				}else if(!FileUtils.extractFileName(zipFileReference).matches(FileUtils.extractFileName(jarFileName)) 
						&& acceptFilesList.indexOf(entryFileName) >= 0){
						//nameFile = FileUtils.takeOffFirstDirectoryPath(jarFileName)+FileUtils.SEPARATOR+entry.getName();
					nameFile = jarFileName+FileUtils.SEPARATOR+entry.getName();
						extractFileName = (jarFileName +FileUtils.SEPARATOR+ entry.getName()).replace(FileUtils.SEPARATOR, "_");
						propertyMap.put(extractFileName, nameFile);//versao nova tentaiva de funcioanmento
						//System.out.println(nameFile);
						ZipUtils.extractFile(zipIn, destDirectory, extractFileName);
						//System.out.println("Arquivo geradoB: "+extractFileName);
				}else
							
						//propertyMap.put(extractFileName, jarFileName + FileUtils.SEPARATOR + entry.getName());//versao cliente antiga
						
					//	System.out.println("passei aaqui"+projectName+"...."+jarFileName+">"+first_time);
						//this.first_time = false;
				
				if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar") || entry.getName().endsWith("war") ||
						entry.getName().endsWith("zip") || entry.getName().endsWith("ear")) {

						if(acceptJARFilesList.indexOf(FileUtils.extractFileName(entry.getName())) >= 0){

								String entryFilePath = FileUtils.extractFilePath(entry.getName());
								
								// Armazena nome completo do arquivo extraido
								String fullFileName = ZipUtils.extractFile(zipIn, destDirectory	+ FileUtils.SEPARATOR + entryFilePath, entryFileName, true);
								//Descompacta o arquivo zip extraido.
								unzipClient(zipFileReference, fullFileName, destDirectory);
								new File(fullFileName).deleteOnExit();
								
								File md5File = new File(fullFileName+".MD5");
								if (md5File.exists()){
									md5File.deleteOnExit();
								}
						}

					
				}

				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
				//this.first_time = false;
			}
			zipIn.close();
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzipClient the EAR file -> "
					+ e.getMessage());
		}
	}
}
