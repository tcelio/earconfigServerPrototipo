package br.com.indra.earconfig.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FilenameUtils;

import br.com.indra.earconfig.utils.CompareUtils;
import br.com.indra.earconfig.utils.FileUtils;
import br.com.indra.earconfig.utils.LogUtils;
import br.com.indra.earconfig.utils.ZipUtils;

public final class ApplyCommand {

	public static final String Configuration_file = "configuration.xml";
	public boolean OVERRIDE = false;
	public boolean ERROR = false;
	public boolean CHANGE_VERIFICATION = false;
	public boolean STOP_ON_VERIFICATION = false;
	private final Map<String, Map<String, String>> jarListFilesMap = new HashMap<String, Map<String, String>>();
	private String configDirectory = "";
	private String folderExtractEar;
	//private static Charset windows_1252 = Charset.forName("windows-1252");//defaultCharset();
	//private static Charset UTF8         = Charset.forName("UTF-8");//defaultCharset();
	//private static Charset CP866        = Charset.forName("CP866");  //("CP866"); //CP437 
	private Charset ENCODE = null;

	private static final String ROOT_FOLDER = ".";

	public ApplyCommand() {
		if (FileUtils.exists(Configuration_file)) {
			if (FileUtils.getXmlConfigurationTag(Configuration_file, "override")) {
				this.OVERRIDE = true;
			}
			if (FileUtils.getXmlConfigurationTag(Configuration_file, "verifyNewFeatures")) {
				this.CHANGE_VERIFICATION = true;
			}
			this.ENCODE = Charset.forName(FileUtils.getXmlConfigurationTagValue(Configuration_file, "encode"));
			
			if (FileUtils.getXmlConfigurationTag(Configuration_file, "stopOnFindNewFeature")) {
				if (this.CHANGE_VERIFICATION = true) {
					this.STOP_ON_VERIFICATION = true;
				} else {
					this.STOP_ON_VERIFICATION = false;
				}

			}
		}
	}

	public void processFile(String configFileZip, String earFile) {
		if (!FileUtils.exists(configFileZip) || FileUtils.isDirectory(configFileZip)) {
			LogUtils.fatal("Invalid file. For command -f, need to be a valid compressed file");
		}
		String extension = configFileZip.substring(configFileZip.length() - 4);

		if (".rar".equalsIgnoreCase(extension)) {
			LogUtils.fatal(
					"Invalid compressed file. A '.rar' file is not valid because RAR is proprietary and now allow other softwares unzip.");
		}

		String generatedName = String.valueOf(Calendar.getInstance().getTimeInMillis());
		String outputFolder = configFileZip.replace(extension, generatedName);
		ZipUtils.unZipIt(configFileZip, outputFolder);

		// Depois de extrair o Zip, eh o mesmo processo do comando D
		// processFolder(outputFolder, earFile);

		try {
			FileUtils.delete(new File(outputFolder));
		} catch (IOException e) {
			LogUtils.debug("Error deleting folder - " + outputFolder + " - " + e.getMessage());
		}
	}

	/**
	 * Comando -D.
	 * 
	 * Com base em uma pasta de configuracao, extrai o EAR, configura e
	 * monta novamente com o nome especificado no arquivo de properties,
	 * parametro "output".
	 * 
	 * @param folder
	 *            - Pasta de arquivos de configuracoes.
	 * @param earFile
	 *            - Nome completo do EAR a ser configurado.
	 */
	public void processFolder(String folder, String earFile, String outputFolder) {
		File folderGenerate = new File(folder);
		File output_folder = new File(outputFolder);
		String extension = FilenameUtils.getExtension(earFile);
		String extension_configuration = "";

		if (extension.matches("ear")||extension.matches("jar")||
				extension.matches("zip")||extension.matches("rar")||
				extension.matches("war")) {
			extension_configuration = "."+extension;
		}

		if (!folderGenerate.isDirectory()) {
			LogUtils.fatal("Invalid directory. For command -d, need to be a directory.");
		}
		if (!output_folder.exists()) {
			System.out.println("output_folder nao existe");
			if (FileUtils.createDirs(output_folder.toString())) {
				System.out.println("output_folder criado com sucesso");
			}
	
		}
		final Map<String, String> propertyMap = FileUtils.loadPropertyFile(folder, ConfigEAR.EAR_CONFIG_FILENAME);

		String earFileName = FileUtils.extractFileName(earFile).replace(extension_configuration, "_");
		extractEARConfigToMap(propertyMap, folder, earFileName, outputFolder);
		System.out.println(
				"\nFinalizado busca de arquivos de referencia!! \nIniciando processo de extraco de arquivos e comparacao com os arquivos de referencia!\nAguarde ...");
		String folderName = FileUtils.extractLastFolder(folder);
		String outputFileName = FileUtils.extractFileName(earFile);

		if (outputFileName == null || outputFileName.isEmpty()) {
			outputFileName = earFileName + folderName + extension_configuration;
		}
		outputFileName = output_folder + FileUtils.SEPARATOR + outputFileName;

		try {
			String generatedName = String.valueOf(Calendar.getInstance().getTimeInMillis());

			folderExtractEar = output_folder + FileUtils.SEPARATOR + earFileName + generatedName;
			configDirectory = folderGenerate.getPath();

			//System.out.println("TDUDO:::" + folderExtractEar + "--" + configDirectory + "---" + outputFileName + "..."+ outputFolder);
			unzipAll(earFile, folderExtractEar, outputFolder);

			generateEarFile(folderExtractEar, outputFileName);

			clearFolder();
		} catch (Exception e) {
			LogUtils.fatal("Error on generate Ear file - " + e.getMessage());
		}
	}

	/**
	 * Monta um MAP baseado nas cofngiuracoes do arquivo
	 * {@link EAR_CONFIG_FILENAME}.
	 * 
	 * @param propertyMap
	 *            - Map com as configuracoes
	 * @param folder
	 *            - Pasta que esta a configuracao
	 * @param earFileName
	 *            - Somente o nome do ear
	 */
	private void extractEARConfigToMap(Map<String, String> propertyMap, String folder, String earFileName,
			String outputFolder) {
		jarListFilesMap.clear();

		for (Entry<String, String> entry : propertyMap.entrySet()) {
			String entryName = null;
//			if (FileUtils.extractFileName(entry.getValue()).matches("com.soluzionasf.arqw10.common_1.0.0.jar")) {
//				System.out.println("\n\n------------------->>>ok\n\n");
//			}
			if (entry.getValue().indexOf(FileUtils.SEPARATOR) >= 0) {
					if (entry.getValue().indexOf(".zip") < 0 && entry.getValue().indexOf(".jar") < 0
							&& entry.getValue().indexOf(".rar") < 0 && entry.getValue().indexOf(".war") < 0
							&& entry.getValue().indexOf(".ear") < 0) {
						entryName = ROOT_FOLDER;
					
					
					} else if (entry.getValue().indexOf(".jar") > 0) {
						entryName = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".jar") + 4);
						System.out.println("JAR>" + entryName);
					
					
					}else if (entry.getValue().indexOf(".rar") > 0) {
						entryName = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".rar") + 4);
						System.out.println("RAR>" + entryName);
					
					
					}else if (entry.getValue().indexOf(".war") > 0) {
						entryName = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".war") + 4);
						System.out.println("WAR>" + entryName);
					

					}else if (entry.getValue().indexOf(".ear") > 0) {
						entryName = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".ear") + 4);
						System.out.println("EAR>" + entryName);
						
						
					}else if (entry.getValue().indexOf(".zip") > 0) {
						entryName = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".zip") + 4);
						System.out.println("ZIP>" + entryName);
					}

			} else {
				entryName = entry.getValue();
			}
			jarListFilesMap.put(entryName, new HashMap<String, String>());
			
		}

		for (Entry<String, String> entry : propertyMap.entrySet()) {
			String entryName = null;
			String nextFolder = entry.getValue();
		
			if (entry.getValue().indexOf(FileUtils.SEPARATOR) >= 0) {
				if (entry.getValue().indexOf(".jar") < 0 && entry.getValue().indexOf(".rar") < 0
						&& entry.getValue().indexOf(".war") < 0 && entry.getValue().indexOf(".ear") < 0
						 && entry.getValue().indexOf(".zip") < 0) {
					entryName  = ROOT_FOLDER;
					nextFolder = entry.getValue().substring(0);
				} else if (entry.getValue().indexOf(".jar") > 0) {

					entryName  = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".jar") + 4);
					nextFolder = entry.getValue().substring(entry.getValue().indexOf(entryName) + entryName.length() + 1);
					System.out.println("Arquivo encontrado: JAR>" + entryName);
				} else if (entry.getValue().indexOf(".rar") > 0) {

					entryName  = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".rar") + 4);
					nextFolder = entry.getValue().substring(entry.getValue().indexOf(entryName) + entryName.length() + 1);
					System.out.println("Arquivo encontrado: RAR>" + entryName);
				} else if (entry.getValue().indexOf(".war") > 0) {

					entryName  = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".war") + 4);
					nextFolder = entry.getValue().substring(entry.getValue().indexOf(entryName) + entryName.length() + 1);
					System.out.println("Arquivo encontrado: WAR>" + entryName);

				} else if (entry.getValue().indexOf(".ear") > 0) {

					entryName  = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".ear") + 4);
					nextFolder = entry.getValue().substring(entry.getValue().indexOf(entryName) + entryName.length() + 1);
					System.out.println("Arquivo encontrado: EAR>" + entryName);

				} else if (entry.getValue().indexOf(".zip") > 0) {

					entryName  = entry.getValue().substring(entry.getValue().indexOf(FileUtils.SEPARATOR) + 1, entry.getValue().indexOf(".zip") + 4);
					nextFolder = entry.getValue().substring(entry.getValue().indexOf(entryName) + entryName.length() + 1);
					System.out.println("Arquivo encontrado: ZIP>" + entryName);

				}


			} else {
				entryName = ROOT_FOLDER;
				nextFolder = entry.getValue().substring(0);
			}
			jarListFilesMap.get(entryName).put(nextFolder, entry.getKey());
		}
	}

	/**
	 * Responsavel por descompactar o EAR, e se o arquivos for um arquivo a
	 * ser alterado, remonta o JAR jar com o arquivo alterado.
	 * 
	 * @param zipFileName
	 *            - Arquivo a ser descompactado
	 * @param destDirectory
	 *            - Diretorio a ser descompactado.
	 */
	private void unzipAll(String zipFileName, String destDirectory, String outputFolder) {
		File destDir = new File(destDirectory);
		if (!destDir.exists()) {
			destDir.mkdir();
		}
		try {
			ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFileName), this.ENCODE);
			ZipEntry entry = zipIn.getNextEntry();
			// Varre todos os arquivos do zip

			while (entry != null) {
				if (entry.isDirectory()) {
					FileUtils.createDirs(destDirectory + FileUtils.SEPARATOR + entry.getName());
				} else if (entry.getName().endsWith(".MD5")) {
					// Nothing
				} else if (!entry.getName().endsWith("jar") && !entry.getName().endsWith("rar")
						&& !entry.getName().endsWith("war") && !entry.getName().endsWith("zip")
						&& !entry.getName().endsWith("ear")) {

					if (jarListFilesMap.get(ROOT_FOLDER).containsKey(entry.getName())) {
						
								LogUtils.debug("Founded - " + entry.getName());
								
								String configuredFile = configDirectory + FileUtils.SEPARATOR + jarListFilesMap.get(ROOT_FOLDER).get(entry.getName());
		
								String target = ZipUtils.extractFile(zipIn, destDirectory, entry.getName(), false);
								if (!FileUtils.checkMD5(configuredFile, target)) {
		
									if (this.OVERRIDE) {
										this.ERROR = true;
										zipIn.closeEntry();
										zipIn.close();
										LogUtils.fatal(
												"Invalid MD5 Check. File modified in new target file: " + entry.getName(),
												this::clearFolder);
									}
									System.out.println("\nArquivo encontrado com modificacoes inesperadas! \nArquivo:: " + configuredFile /* +zipFileName */);
									if (this.CHANGE_VERIFICATION) {
		
										verificarMudancas(destDirectory + FileUtils.SEPARATOR + entry.getName(), outputFolder,
												entry.getName());
		
									}
		
								}
								FileUtils.copy(configuredFile, target);

						// nao esteja na lista de procurados para avaliacao...
					} else {
						ZipUtils.extractFile(zipIn, destDirectory, entry.getName(), false);

					}

				} else if (entry.getName().endsWith("jar") || entry.getName().endsWith("rar")
						|| entry.getName().endsWith("war") || entry.getName().endsWith("zip")
						|| entry.getName().endsWith("ear")) {

					String fullFileName = ZipUtils.extractFile(zipIn, destDirectory, entry.getName(), false);

					// Se o jar esta configurado, recria editando os arquivos
					// configurados
					if (jarListFilesMap
							.containsKey((FileUtils.takeOffFirstDirectoryPathPlus(entry.getName()).substring(0)))) {
						configNewJarFile(fullFileName,
								(FileUtils.takeOffFirstDirectoryPathPlus(entry.getName()).substring(0)), outputFolder);
					}

				}
				zipIn.closeEntry();
				entry = zipIn.getNextEntry();
			}
			zipIn.close();
			System.out.println("\n\nfinalizando processo de re-empacotamento..\naguarde");
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.fatal("Error on unzip the EAR file -> " + e.getMessage());
			System.out.println("Error on unzip the EAR file -> " + e.getMessage() + "--" + e.getCause());
		}
	}

	/**
	 * Responsavel por varrer o Map de arquivos configurados, encontrar no JAR
	 * e remontar com o novo arquivo.
	 * 
	 * @param fullFileName
	 *            - Nome completo do caminho do Jar.
	 * @param jarFileName
	 *            - Somente nome do Jar.
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private void configNewJarFile(String fullFileName, String jarFileName, String outputFolder)
			throws IOException, FileNotFoundException {

		String novo_nome = "";
		String filePath = "";
		// ArrayList<String> diff_list = new ArrayList<String>();

		if (!jarListFilesMap.containsKey(jarFileName)) {
			return;
		}

		Map<String, String> mapFiles = jarListFilesMap.get(jarFileName);

		// Copia jar original para ler registro a registro
		String fullFileNameOriginal = fullFileName + ZipUtils.SUFFIX_COPY;
		FileUtils.copy(fullFileName, fullFileNameOriginal);

		ZipFile zipFile = new ZipFile(fullFileNameOriginal);
		final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(fullFileName));

		for (Enumeration<? extends ZipEntry> e = zipFile.entries(); e.hasMoreElements();) {
			ZipEntry entryIn = e.nextElement();

//			if ((FileUtils.extractFileName(entryIn.getName())).matches("notifiers.xml")) {
//				System.out.println("Arquivo analisado: " + entryIn.getName());
//			}

			boolean diff_list = false;

			if (mapFiles.containsKey(entryIn.getName())) {
				LogUtils.debug("FOUNDED - " + mapFiles.get(entryIn.getName()) + " - to " + entryIn.getName());

				String configuredFile = configDirectory + FileUtils.SEPARATOR + mapFiles.get(entryIn.getName());
				String target = configDirectory + FileUtils.SEPARATOR + "target.file";

				ZipUtils.extractFile(zipFile.getInputStream(entryIn), configDirectory, "target.file");

				if (!FileUtils.checkMD5(configuredFile, target)) {

					System.out.println("\nEncontrado mudancas de checksum MD5 relacionado ao arquivo: \n"	+ fullFileName + "->" + configuredFile);
					if (this.OVERRIDE) {
						this.ERROR = true;
						zos.closeEntry();
						zos.close();
						zipFile.close();
						// Mark copy to delete after finish program
						new File(fullFileNameOriginal).deleteOnExit();
						LogUtils.fatal("Invalid MD5 Check.. File modified in new target file: " + entryIn.getName(), this::clearFolder);
					}
					if (this.CHANGE_VERIFICATION) {
						System.out.println("Iniciado processo de verificacao por mudancas de funcionalidades!\nAguarde");
						//String fileReferencia = FileUtils.tratarStringPath(mapFiles.get(entryIn.getName()));
					
						int h = entryIn.getName().indexOf(".");
						if (h != -1) {// verifica se o arquivo com o "." existe.
										// Pode ser que o arquivo nao tenha "."
							String fileExtension = FilenameUtils.getExtension(entryIn.getName());

							File one = new File(fullFileNameOriginal);
							File two = new File("cache" + FileUtils.SEPARATOR + fullFileNameOriginal);
							two.deleteOnExit();
							try {
								// rename
								String absolutePath = two.toString();
								filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
								FileUtils.createDirs(filePath);
								if (!two.exists()) {
									// copia o arquivo para o nov local
									Files.copy(one.toPath(), two.toPath());
								}

								novo_nome = two.toString().substring(0, two.toString().length() - 9);
								boolean ok = false;
								try {
									File newNome_NewPath = new File(novo_nome);
									//newNome_NewPath.deleteOnExit();
									ok = two.renameTo(newNome_NewPath);//tentativa de renomear
									if (ok) {
										ZipUtils.extractFactory(novo_nome, filePath);
									}
								} catch (Exception e2) {
									System.out.println("Falha no preocesso de renomear arquivo de referencia!");
									System.out.println("Error::: " + e2.getMessage() + "--" + e2.getCause());
								}

								
								if (fileExtension.matches("xml")) {
									diff_list = CompareUtils.verificarTipoDeDiferencaXML(configuredFile, filePath + FileUtils.SEPARATOR + entryIn.getName());
								} else if (fileExtension.matches("properties")) {
									diff_list = CompareUtils.verificarTipoDeDiferencaPROPERTIES(configuredFile, filePath + FileUtils.SEPARATOR + entryIn.getName());
								} else if (fileExtension.matches("cfg")) {
									diff_list = CompareUtils.verificarTipoDeDiferencaCFG(configuredFile, filePath + FileUtils.SEPARATOR + entryIn.getName());
								}

							} catch (Exception e2) {
								System.out.println("Error: " + e2.getMessage() + "--" + e2.getCause() + "--"+ e2.getLocalizedMessage());
								System.out.println("Error:::: " + e2.getStackTrace());
							} catch (ExceptionInInitializerError e1) {
								System.out.println(	"Error: " + e1.getMessage() + "--" + e1.getCause() + "--" + e1.getStackTrace());
								System.out.println("Error:: " + e1.getClass());

							} catch (Error e3) {
								System.out.println("Error: " + e3.getMessage() + "--" + e3.getCause() + "--" + e3.getStackTrace());
								System.out.println("Error::: " + e3.getClass());

							} finally {
								// FileUtils.delete(new File(novo_nome));
							}

						}

						if (!diff_list) {
							System.out.println("\n NAO ENCONTRADO DIFERENCA NO XML/CFG/PROPERTIES RELACIONADO A ADICAO DE FUNCIONALIDADES!\n");
						}
						if (diff_list && this.STOP_ON_VERIFICATION) {
							this.ERROR = true;
							zos.closeEntry();
							zos.close();
							zipFile.close();
							LogUtils.fatal("Invalid MD5 Check and new functionality found. File modified in new ear file: "+ entryIn.getName(), this::clearFolder);
						}
					}
			}
				ZipUtils.addFileToZip(zos, configDirectory, mapFiles.get(entryIn.getName()), entryIn.getName());
			} else {
				//LINHA ABAIXO ADICIONADA PARA RESOLVER PROBLEMA DE FALHA DE TAMANHO DE ZIP 
				ZipEntry newEntry = new ZipEntry(entryIn.getName());
				zos.putNextEntry(newEntry);
				InputStream is = zipFile.getInputStream(entryIn);
				byte[] buf = new byte[1024];
				int len;
				while ((len = (is.read(buf))) > 0) {
					zos.write(buf, 0, len);
				}
			}
			zos.closeEntry();
		}
		zos.close();
		zipFile.close();

		// Mark copy to delete after finish program
		new File(fullFileNameOriginal).deleteOnExit();
	}


	public void verificarMudancas(String fullFileName, String outputFolder, String nameFile) throws IOException {
		String novo_nome = "";
		String filePath = "";
		String filePath2 = "";
		String fileType = "";
		boolean diff_list = false;// utilizado originalmente em cada momento do
									// loop
		String hhh = nameFile.replace("/", "_");
		String configuredFile = configDirectory + FileUtils.SEPARATOR + hhh;
		// Copia jar original para ler registro a registro
		String fullFileNameOriginal = fullFileName + ZipUtils.SUFFIX_COPY;
		FileUtils.copy(fullFileName, fullFileNameOriginal);
		System.out.println("Iniciado processo de verificacao por mudancas de funcionalidades!\nAguarde");
	
		String fileReferencia = FileUtils.tratarStringPath(nameFile);
		
		String fileExtension = FilenameUtils.getExtension(nameFile);

		File one = new File(fullFileNameOriginal);
		File two = new File("cache/" + fullFileNameOriginal);
		two.deleteOnExit();
		try {
			// rename
			String absolutePath = two.toString();
			filePath2 = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
			filePath = filePath2.substring(0, filePath2.lastIndexOf(File.separator));
			FileUtils.createDirs(filePath2);
			// copia o arquivo para o nov local
			try {
				Files.copy(one.toPath(), two.toPath());
			} catch (Exception e) {
				System.out.println("Erro :" + e.getMessage() + "-" + e.getCause() + "--" + e.getSuppressed());
			}

			novo_nome = two.toString().substring(0, two.toString().length() - 9);
	
			// renomear
			File newNome_NewPath = new File(novo_nome);
			boolean ok = two.renameTo(newNome_NewPath);// tentativa de renomear
														// o arquivos alvo
	
			if (fileExtension.matches("xml")) {
				//System.out.println("\n\n:::Mudancas a verificar:::"+configuredFile+"---"+fullFileName);
				diff_list = CompareUtils.verificarTipoDeDiferencaXML(configuredFile, fullFileName);
				fileType = "XML";
			} else if (fileExtension.matches("properties")) {
				diff_list = CompareUtils.verificarTipoDeDiferencaPROPERTIES(configuredFile, fullFileName);// filePath+"/"+nameFile);
				fileType = "PROPERTIES";
			} else if (fileExtension.matches("cfg")) {
				fileType = "CFG";
				diff_list = CompareUtils.verificarTipoDeDiferencaCFG(configuredFile, fullFileName);
			}

		} catch (Exception e2) {
			System.out.println("Error: " + e2.getMessage());
			System.out.println("Error: " + e2.getCause());
			System.out.println("Error: " + e2.getMessage() + "--" + e2.getCause() + "--" + e2.getStackTrace());
		} finally {
			// FileUtils.delete(new File(novo_nome));
		}

	

		if (!diff_list) {
			System.out.println("\n NAO ENCONTRADO DIFERENCA NO XML RELACIONADO A ADICAO DE FUNCIONALIDADES!\n");
		}
		if (diff_list && this.STOP_ON_VERIFICATION) {
			this.ERROR = true;
			LogUtils.fatal(
					"Invalid MD5 Check and new functionality found. File modified in new ear file: " + configuredFile,
					this::clearFolder);
			System.exit(0);
		}

	}

	/**
	 * Gera uma arquivo EAR com base na pasta e com nome expecificado no output.
	 * 
	 * @param folderExtractedEar
	 *            - Pasta onde estao os arquivos a serem empacotados.
	 * @param outputFileName
	 *            - Nome completo do novo EAR a ser gerado.
	 * @throws Exception
	 */
	private void generateEarFile(String folderExtractedEar, String outputFileName) throws Exception {
		ZipUtils.zipFolder(folderExtractedEar, outputFileName);
		System.out.println("finalizando processo de re-criacao do arquivo principal...\naguarde");
	}

	public void clearFolder() {
		try {
			FileUtils.delete(new File(folderExtractEar));
			System.out.println("finalizando limpeza de cache...\naguarde");
			if (this.ERROR) {
				System.out.println("\nAplicacao abortada!!!");
			} else {
				System.out.println("\nAplicacao finalizada com sucesso!");
			}

		} catch (IOException e) {
			LogUtils.debug("Error deleting folder - " + folderExtractEar + " - " + e.getMessage());
		}
	}
}
